﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace simple_con_app
{
    class Program
    {
        private static readonly NLog.Logger LOG = NLog.LogManager.GetCurrentClassLogger();

        [Obsolete]
        static void Main(string[] args)
        {
            NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(@"res\nlog.config", true);

            while (true)
            {
                LOG.Info("Press [q] to quit...");
                var cki = Console.ReadKey();
                if (cki.KeyChar == 'q')
                    break;
                LOG.Info($"Pressed {cki.KeyChar}");
                Thread.Sleep(200);
                //Thread.Sleep(500);
            }

        }
    }
}
